package org.vadel.libs.parse.engine;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by VBabin on 17.12.2015.
 */
public class GetStringParse extends GetString {

    static String[] getArr(JSONObject obj, String name) throws JSONException {
        if (!obj.has(name))
            return null;
        Object o = obj.get(name);
        if (o instanceof String)
            return new String[] { (String) o };
        else if (o instanceof JSONArray) {
            JSONArray arr = (JSONArray) o;
            String[] res = new String[arr.length()];
            for (int i = 0; i < arr.length(); i++) {
                res[i] = arr.getString(i);
            }
            return res;
        } else
            return null;
    }

    String before, tag, token1, token2, stop;
    String[] after, skip;

    ReplacerString replacer;

    public GetStringParse(JSONObject obj) throws JSONException {
        this.stop = JSON.getStringSafe(obj, "stop");
        this.tag = JSON.getStringSafe(obj, "tag");
        this.after = getArr(obj, "after");
        this.skip = getArr(obj, "skip");
        this.before = JSON.getStringSafe(obj, "before");
        this.token1 = JSON.getStringSafe(obj, "token1");
        this.token2 = JSON.getStringSafe(obj, "token2");
        this.replacer = ReplacerString.fromJson(obj, "replace");
    }

    @Override
    public boolean isValid() {
        return token1 != null && token2 != null;
    }

    public String getValue(StringBuilder str) {
        return getValue(str, 0);
    }

    public String getValue(StringBuilder str, int i) {
        int i1 = i, i2;
        if (tag != null) {
            i1 = str.indexOf(tag, i);
            if (i1 >= 0)
                i1 = str.lastIndexOf("<", i1);
        } else if (after != null) {
            for (int j = 0; j < after.length && i1 >= 0; j++) {
                i1 = str.indexOf(after[j], i1);
            }
        } else if (skip != null) {
            for (int j = 0; j < skip.length && i1 >= 0; j++) {
                String skp = skip[j];
                i1 = str.indexOf(skp, i1);
                if (i1 >= 0) {
                    i1 += skp.length();
                }
            }
        } else if (before != null) {
            i1 = str.indexOf(before, i);
        }

        if (i1 < 0)
            return null;
        int stopIndex = -1;
        if (before != null) {
            if (stop != null)
                stopIndex = str.lastIndexOf(stop, i1);

            i1 = str.lastIndexOf(token1, i1);
            if (i1 < 0 || (stopIndex >= 0 && stopIndex > i1))
                return null;
        } else {
            if (stop != null)
                stopIndex = str.indexOf(stop, i1);

            i1 = str.indexOf(token1, i1);
            if (i1 < 0 || (stopIndex >= 0 && stopIndex < i1))
                return null;
        }

        i1 += token1.length();
        i2 = str.indexOf(token2, i1);
        if (i1 >= 0 && i2 >= 0) {
            return replaceIfNeeded(str.substring(i1, i2)).trim();
        }
        return null;
    }

    String replaceIfNeeded(String s) {
        if (replacer != null)
            s = replacer.replaceIfNeeded(s);
        return s.trim();
    }
/*
    @Deprecated
    public List<String> getValues(StringBuilder str, int i) {
        List<String> result = new ArrayList<String>();
        String after;// = this.after;
        if (this.after != null)
            after = this.after[0];
        else //if (after == null)
            after = token1;

        i = str.indexOf(after, i);
        while (i >= 0) {
            String s = ParseHelper.getLineValueSB(str, after, token1, token2, i);
            if (s == null)
                break;
            result.add(replaceIfNeeded(s));
            i = str.indexOf(after, i + after.length());
        }
        return result;
    }
*/
}
