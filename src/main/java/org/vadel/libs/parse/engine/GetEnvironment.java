package org.vadel.libs.parse.engine;

import java.util.HashMap;
import java.util.Map;

public class GetEnvironment {

	public static final String START = "%%";
	public static final String END   = "%%";
	
	public static GetEnvironment getEnvironment = new GetEnvironment();
	
	Map<String, String> values = new HashMap<String, String>();
	
	public synchronized void setVar(String name, String value) {
		values.put(name, value);
	}
	
	public synchronized String getValue(String name) {
		return values.get(name);
	}
	
	public synchronized String replaceVars(String s) {
		for (String name : values.keySet()) {
			String var = START + name + END;
			if (s.contains(var))
				s = s.replace(var, values.get(name));
		}
		return s;
	}
}
