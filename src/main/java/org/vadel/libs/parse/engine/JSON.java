package org.vadel.libs.parse.engine;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class JSON {

    public static String getStringFromFile(File f) throws IOException {
        if (!f.exists() || f.isDirectory())
            return null;
        StringBuilder str = new StringBuilder();
        int n;
        FileInputStream fin = new FileInputStream(f);
        byte[] buff = new byte[1024];
        while ((n = fin.read(buff)) > 0) {
            str.append(new String(buff, 0, n, "utf-8"));
        }
        fin.close();
        return str.toString();
    }

    public static JSONObject fromFile(File f) throws IOException, JSONException {
        String s = getStringFromFile(f);
        if (s == null)
            return null;
        return new JSONObject(s);
    }

    public static String getStringSafe(JSONObject obj, String name, String def) throws JSONException {
        if (obj.has(name))
            return obj.getString(name);
        else
            return def;
    }

    public static String getStringSafe(JSONObject obj, String name) throws JSONException {
        if (obj.has(name))
            return obj.getString(name);
        else
            return null;
    }

    public static int getIntSafe(JSONObject obj, String name, int def) throws JSONException {
        if (obj.has(name))
            return obj.getInt(name);
        else
            return def;
    }

    public static boolean getBooleanSafe(JSONObject obj, String name, boolean def) throws JSONException {
        if (obj.has(name))
            return obj.getBoolean(name);
        else
            return def;
    }
    
    public static Object getSafe(JSONObject obj, String name) throws JSONException {
        if (obj.has(name))
            return obj.get(name);
        else
            return null;
    }

    public static JSONObject getObjectSafe(JSONObject obj, String name) throws JSONException {
        if (obj.has(name))
            return obj.getJSONObject(name);
        else
            return null;
    }

    public static JSONObject getObjectSafe(JSONObject obj, String name, JSONObject def) throws JSONException {
        if (obj.has(name))
            return obj.getJSONObject(name);
        else
            return def;
    }

    public static JSONArray getArraySafe(JSONObject obj, String name) throws JSONException {
        if (obj.has(name))
            return obj.getJSONArray(name);
        else
            return null;
    }

	public static long getLongSafe(JSONObject obj, String name, long def) throws JSONException {
        if (obj.has(name))
            return obj.getLong(name);
        else
            return def;
	}

	public static Object get(JSONObject obj, String name) throws JSONException {
        if (obj.has(name))
            return obj.get(name);
        else
        	return null;
	}
	
	public static String[] getStringArray(JSONObject obj, String name) throws JSONException {
		if (!obj.has(name))
			return null;
		Object o = JSON.get(obj, name);
		if (o instanceof String) {
			return new String[] { (String) o };
		} else if (o instanceof JSONArray) {
			JSONArray arr = (JSONArray) o;
			String[] strings = new String[arr.length()];
			for (int i = 0; i < arr.length(); i++)
				strings[i] = arr.getString(i);
			return strings;
		}		
		return null;
	}
}
