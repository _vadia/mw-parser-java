package org.vadel.libs.parse.engine;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by VBabin on 17.12.2015.
 */
public class GetStringBuild extends GetString {

    final Map<String, GetString> vars = new HashMap<String, GetString>();
    final String buildString;

    public GetStringBuild(JSONObject obj) throws JSONException {
        buildString = JSON.getStringSafe(obj, "build");
        Object o = JSON.get(obj, "values");
        if (o instanceof JSONArray) {
            JSONArray valuesArr = (JSONArray) o;
            for (int i = 0; i < valuesArr.length(); i++) {
                Object var = valuesArr.get(i);
                if (var instanceof JSONObject) {
                    JSONObject varObj = (JSONObject) var;
                    String name = JSON.getStringSafe(varObj, "name");
                    GetString getString = createFromObject(JSON.get(varObj, "value"));
                    if (getString != null)
                        vars.put(name, getString);
                }
            }
        } else if (o instanceof JSONObject) {
            JSONObject valuesObj = (JSONObject) o;
            JSONArray names = valuesObj.names();
            for (int i = 0; i < names.length(); i++) {
                String name = names.getString(i);
                GetString getString = createFromObject(valuesObj.get(name));
                if (getString != null)
                    vars.put(name, getString);
            }
        }
    }

    @Override
    public boolean isValid() {
        return buildString != null;
    }

    @Override
    public String getValue(StringBuilder str, int i) {
        if (buildString == null || buildString.length() == 0)
            return null;

        String s = buildString;
        for (String varName : vars.keySet()) {
            GetString varGetString = vars.get(varName);
            s = s.replace(GetEnvironment.START + varName + GetEnvironment.END, varGetString.getValue(str, i));
        }

        return s;
    }
/*
    @Override
    public List<String> getValues(StringBuilder str, int i) {
        return null;
    }
*/
}
