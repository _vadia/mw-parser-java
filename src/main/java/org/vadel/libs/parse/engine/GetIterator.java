package org.vadel.libs.parse.engine;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GetIterator {

    public static GetIterator makeIterator(JSONObject obj, String name) throws JSONException {
        if (obj.has(name))
            return new GetIterator(obj, name);
        return null;
    }

    String url;

    String[] urls;

    String baseUrl;
    String[] appendStr;
    int iterateFrom = 0, iterateTo = -1, iterateStep = 1;
    String sufix;

    public GetIterator(JSONObject obj, String name) throws JSONException {
        Object o = JSON.get(obj, name);
        if (o instanceof String) {
            url = (String) o;
        } else if (o instanceof JSONObject) {
            fromJSON((JSONObject) o);
        } else if (o instanceof JSONArray) {
            JSONArray arr = (JSONArray) o;
            urls = new String[arr.length()];
            for (int i = 0; i < arr.length(); i++)
                urls[i] = arr.getString(i);
        }
    }

    public GetIterator(JSONObject obj) throws JSONException {
        fromJSON(obj);
    }


    void fromJSON(JSONObject obj) throws JSONException {
        baseUrl = JSON.getStringSafe(obj, "base_url");
        JSONObject appendNums = JSON.getObjectSafe(obj, "append_nums");
        JSONArray appendArray = JSON.getArraySafe(obj, "append_array");

        if (appendNums != null) {
            iterateFrom = JSON.getIntSafe(appendNums, "from", iterateFrom);
            iterateTo = JSON.getIntSafe(appendNums, "to", iterateTo);
            iterateStep = JSON.getIntSafe(appendNums, "step", iterateStep);
//			prefix      = JSON.getStringSafe(appendNums, "prefix", ""); 
            sufix = JSON.getStringSafe(appendNums, "sufix", "");
        }
        if (appendArray != null) {
            appendStr = new String[appendArray.length()];
            for (int i = 0; i < appendArray.length(); i++)
                appendStr[i] = appendArray.getString(i);
        }
    }

    int indexNext = -1;

    public void reset() {
        indexNext = -1;
    }

    public boolean next() {
        if (indexNext < 0)
            indexNext = iterateFrom;
        else
            indexNext += iterateStep;
        if (url != null) {
            return indexNext == 0;
        } else if (urls != null) {
            return indexNext < urls.length;
        } else if (appendStr != null) {
            return indexNext < appendStr.length;
        } else {
            return indexNext <= iterateTo;
        }
    }

    public String get() {
        if (url != null) {
            if (indexNext == 0)
                return url;
            return null;
        } else if (urls != null) {
            return urls[indexNext];
        } else if (appendStr != null) {
            return baseUrl + appendStr[indexNext];
        } else {
            return baseUrl + indexNext + sufix;
        }
    }
}
