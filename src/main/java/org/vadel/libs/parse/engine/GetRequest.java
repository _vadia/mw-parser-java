package org.vadel.libs.parse.engine;

import java.net.HttpURLConnection;
import java.net.ProtocolException;

import org.json.JSONException;
import org.json.JSONObject;

public class GetRequest {

	public String method;
	public String jsonField;
	public String[] headers;
	
	public GetRequest(JSONObject obj) throws JSONException {
		this.method    = JSON.getStringSafe(obj, "method");
		this.jsonField = JSON.getStringSafe(obj, "json");
//		this.headers   = JSON.getStringArray(obj, "headers");
	}
	
	public void setRequest(HttpURLConnection httpConnection) throws ProtocolException {
		if (method != null)
			httpConnection.setRequestMethod(method);
		for (String s : headers) {
			String[] ss = s.split(":");
			if (ss.length != 2)
				continue;
			httpConnection.setRequestProperty(ss[0], ss[1]);
		}
	}
}
