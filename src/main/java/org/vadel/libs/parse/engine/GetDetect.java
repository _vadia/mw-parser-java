package org.vadel.libs.parse.engine;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GetDetect {

    public static GetDetect createDetect(JSONObject obj, String name) {
        if (obj == null)
            return null;
        try {
            Object o = JSON.getSafe(obj, name);

            if (o == null)
                return null;
            return createDetect(o);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static GetDetect createDetect(Object o) {
        try {
            if (o instanceof JSONObject) {
                return new GetDetect((JSONObject) o);
            } else if (o instanceof JSONArray) {
                JSONArray arr = (JSONArray) o;
                return new GetDetect(arr);
            } else {
                return new GetDetect((String) o);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private final String start;
    private final String end;
    final String value;
    private final String[] values;
    private final Pattern pattern;

    public GetDetect(JSONObject obj) throws JSONException {
        this.start = JSON.getStringSafe(obj, "start");
        this.end = JSON.getStringSafe(obj, "end");
        this.value = JSON.getStringSafe(obj, "value");
        this.values = arrayToStrings(JSON.getArraySafe(obj, "values"));
        String patterns = JSON.getStringSafe(obj, "pattern");
        this.pattern = patterns != null ? Pattern.compile(patterns) : null;
    }

    public GetDetect(JSONArray arr) throws JSONException {
        this.start = null;
        this.end = null;
        this.value = null;
        this.values = arrayToStrings(arr);
        this.pattern = null;
    }

    public GetDetect(String value) {
        this.start = null;
        this.end = null;
        this.value = value;
        this.values = null;
        this.pattern = null;
    }

    String[] arrayToStrings(JSONArray arr) throws JSONException {
        if (arr == null)
            return null;
        String[] patterns = new String[arr.length()];
        for (int i = 0; i < arr.length(); i++)
            patterns[i] = arr.getString(i);
        return patterns;
    }

    public boolean detect(StringBuilder str) {
        return detect(str, 0);
    }

    public boolean detect(StringBuilder str, int i) {
        return getIndex(str, i) >= 0;
    }

    public boolean detect(StringBuilder str, int st, int en) {
        return getIndex2(str, st, en) >= 0;
    }

    public int getIndex(StringBuilder str) {
        return getIndex(str, 0);
    }

    public int getIndex(StringBuilder str, int i) {
        if (pattern != null) {
            Matcher m = pattern.matcher(str.toString());//.start(i);
            if (m.find(i))
                return m.start();
            else
                return -1;
        }

        int en = str.length();
        if (start != null)
            i = str.indexOf(start, i);
        if (i < 0)
            return -1;
        if (end != null)
            en = str.indexOf(end, i);
        if (en <= 0)
            return -1;

        if (values != null) {
            for (int k = 0; k < values.length; k++) {
                int j = str.indexOf(values[k], i);
                if (j >= 0 && j < en)
                    return j;
            }
        } else {
            int j = str.indexOf(value, i);
            if (j >= 0 && j < en)
                return j;
        }
        return -1;
    }

    public int getIndex2(StringBuilder str, int st, int en) {
        if (pattern != null) {
            Matcher m = pattern.matcher(str.toString());//.start(i);
            if (m.find(st))
                return m.start();
            else
                return -1;
        }

        if (start != null)
            st = str.indexOf(start, st);
        if (st < 0)
            return -1;

        if (en <= 0)
            en = str.length();

        if (end != null) {
            int en2 = str.indexOf(end, st);
            if (en2 <= 0 || en2 > en)
                return -1;
            en = en2;
        }
//        if (en <= 0)
//            return -1;

        if (values != null) {
            for (int k = 0; k < values.length; k++) {
                int j = str.indexOf(values[k], st);
                if (j >= 0 && j < en)
                    return j;
            }
        } else {
            int j = str.indexOf(value, st);
            if (j >= 0 && j < en)
                return j;
        }
        return -1;
    }
}