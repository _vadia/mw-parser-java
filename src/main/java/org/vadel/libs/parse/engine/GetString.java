package org.vadel.libs.parse.engine;

import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("unused")
public abstract class GetString {

    public static GetString createString(JSONObject obj, String name) {
        if (obj == null)
            return null;
        try {
            return createFromObject(JSON.getSafe(obj, name));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static GetString createFromObject(Object o) {
        if (o == null)
            return null;
        try {
            if (o instanceof JSONObject) {
                JSONObject obj = (JSONObject) o;
                String kind = JSON.getStringSafe(obj, "kind");
                if (kind == null || kind.equals("parse"))
                    return new GetStringParse(obj);
                else if (kind.equals("vars"))
                    return new GetStringBuild(obj);
            } else if (o instanceof String) {
                return new GetConstantString((String) o);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public abstract boolean isValid();

    public String getValue(StringBuilder str) {
        return getValue(str, 0);
    }

    public abstract String getValue(StringBuilder str, int i);
/*
    @Deprecated
    public abstract List<String> getValues(StringBuilder str, int i);
*/
}