package org.vadel.libs.parse.engine;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.vadel.libs.parse.ParseHelper;

public class GetArrayString {

    public static GetArrayString createArrayString(JSONObject obj, String name) {
        if (obj == null)
            return null;
        try {
            JSONObject o = JSON.getObjectSafe(obj, name);
            if (o == null)
                return null;
            return new GetArrayString(o);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    String after, split;
    String start, end;
    String token1, token2;

    ReplacerString replacer;
    boolean details;

    GetArrayString(JSONObject obj) throws JSONException {
        this.start = JSON.getStringSafe(obj, "start");
        this.end   = JSON.getStringSafe(obj, "end");

        this.after = JSON.getStringSafe(obj, "after");
        this.split = JSON.getStringSafe(obj, "split");

        this.token1 = JSON.getStringSafe(obj, "token1");
        this.token2 = JSON.getStringSafe(obj, "token2");

        this.details = JSON.getBooleanSafe(obj, "details", details);
        this.replacer = ReplacerString.fromJson(obj, "replace");
    }

    public void getValue(List<String> values, StringBuilder str) {
        getValue(values, str, 0);
        return;
    }

    public void getValue(List<String> values, StringBuilder str, int i) {
        if (split != null) {
            if (start != null)
                i = str.indexOf(start, i);
            if (i < 0)
                return;

            if (after == null)
                after = token1;
            String line = null;
            if (token1 != null && token2 != null)
                line = ParseHelper.getLineValueSB(str, after, token1, token2, i);
            else if (str.length() < 20000) {
                line = str.toString();
            }
            if (line != null) {
                String[] arr = line.split(split);
                for (int j = 0; j < arr.length; j++) {
                    String s = arr[j];
                    s = replaceIfNeeded(s);
                    if (details)
                        System.out.println(String.format("[GetArrayString.add]%s", s));
                    values.add(s);
                }
            }
        } else {
            List<String> list = new ArrayList<String>();
            ParseHelper.getPagesList(str, list, start, token1, token2, end, "", "");
            for (int k = 0; k < list.size(); k++) {
                String s = replaceIfNeeded(list.get(k));
                if (details)
                    System.out.println(String.format("[GetArrayString.add]%s", s));
                values.add(s);
            }
        }
        return;
    }

    String replaceIfNeeded(String s) {
        if (replacer != null)
            s = replacer.replaceIfNeeded(s);
        return s.trim();
    }
}