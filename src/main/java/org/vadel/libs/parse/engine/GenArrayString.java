package org.vadel.libs.parse.engine;

import org.json.JSONException;
import org.json.JSONObject;

public class GenArrayString {

    public static String VAR_INDEX = "index";

    public static GenArrayString createGenArray(JSONObject obj, String name) throws JSONException {
        Object o = JSON.get(obj, name);
        if (o == null)
            return null;
        if (o instanceof JSONObject)
            return new GenArrayString((JSONObject) o);
        else if (o instanceof String)
            return new GenArrayString((String) o, 0);
        else
            return null;
    }

    public int start;
    public String pattern;

    public GenArrayString(JSONObject obj) throws JSONException {
        this.pattern = JSON.getStringSafe(obj, "pattern");
        this.start = JSON.getIntSafe(obj, "start", start);
    }

    public GenArrayString(String pattern, int start) throws JSONException {
        this.pattern = pattern;
        this.start = start;
    }

    public String getValue(int index) {
        GetEnvironment.getEnvironment.setVar(VAR_INDEX, String.valueOf(start + index));
        return GetEnvironment.getEnvironment.replaceVars(pattern);
    }
}
