package org.vadel.libs.parse.engine;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ReplacerString {

    public static ReplacerString[] createCorrectionArray(JSONObject obj, String name) throws JSONException {
        Object o = JSON.get(obj, name);
        if (o != null) {
            if (o instanceof JSONObject) {
                return new ReplacerString[]{ new ReplacerString((JSONObject) o) };
            } else if (o instanceof JSONArray) {
                JSONArray arr = (JSONArray) o;
                ReplacerString[] result = new ReplacerString[arr.length()];
                for (int i = 0; i < arr.length(); i++) {
                    result[i] = new ReplacerString(arr.getJSONObject(i));
                }
                return result;
            }
        }
        return null;
    }

    public static ReplacerString fromJson(JSONObject obj, String name) throws JSONException {
        Object o = JSON.get(obj, name);
        if (o != null) {
            if (o instanceof JSONObject) {
                return new ReplacerString((JSONObject) o);
            } else if (o instanceof JSONArray) {
                return new ReplacerString((JSONArray) o);
            }
        }
        return null;

    }

    SimpleReplacerString[] replacers;

    private ReplacerString(JSONObject obj) throws JSONException {
        this.replacers = new SimpleReplacerString[]{new SimpleReplacerString(obj)};
    }

    private ReplacerString(JSONArray arr) throws JSONException {
        this.replacers = new SimpleReplacerString[arr.length()];// { new SimpleReplacerString(obj) };
        for (int i = 0; i < arr.length(); i++) {
            this.replacers[i] = new SimpleReplacerString(arr.getJSONObject(i));
        }
    }

    public String replaceIfNeeded(String s) {
        for (int i = 0; i < replacers.length; i++)
            s = replacers[i].replaceIfNeeded(s);
        return s;
    }

    static class SimpleReplacerString {

        String replaceMatch, replaceText;
        String prefix, sufix;

        private SimpleReplacerString(JSONObject obj) throws JSONException {
            this.replaceMatch = JSON.getStringSafe(obj, "match");
            this.replaceText  = JSON.getStringSafe(obj, "text");
            this.prefix       = JSON.getStringSafe(obj, "prefix");
            this.sufix        = JSON.getStringSafe(obj, "sufix");
        }

        public String replaceIfNeeded(String s) {
            if (replaceMatch != null && replaceText != null)
                s = s.replaceAll(replaceMatch, replaceText);
            if (prefix != null)
                s = prefix + s;
            if (sufix != null)
                s += sufix;
            return GetEnvironment.getEnvironment.replaceVars(s).trim();
        }
    }
}