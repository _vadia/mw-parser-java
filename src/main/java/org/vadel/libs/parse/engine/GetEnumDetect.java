package org.vadel.libs.parse.engine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("unused")
public class GetEnumDetect {


    public static GetEnumDetect createEnumDetect(JSONObject obj, String name, String[] values) throws JSONException {
        Object o = JSON.get(obj, name);
        if (o instanceof JSONObject) {
            return new GetEnumDetect((JSONObject) o, values);
        } else if (o instanceof JSONArray) {
            return new GetEnumDetect((JSONArray) o, values);
        } else {
            return null;
        }
    }

    private final List<Holder> detectors = new ArrayList<GetEnumDetect.Holder>();

//    private int defaultValue = -1;
//    private String defaultName;
    private Holder defaultEnum;
    private GetDetect getStart, getEnd;
    private boolean returnName;
    private boolean details;

    public GetEnumDetect(JSONObject obj, String[] values) throws JSONException {
        List<String> list = Arrays.asList(values);
        JSONArray names = obj.names();
        String defaultStr = JSON.getStringSafe(obj, "default");
        for (int i = 0; i < names.length(); i++) {
            String key = names.getString(i);
            if (key.equals("default"))
                continue;
            int index = list.indexOf(key);
            if (index < 0) {
                System.out.println("[GetEnumDetect] Warning! Unknown enum item: " + key);
                continue;
            }
            Holder holder = new Holder(index, key, GetDetect.createDetect(obj, key));
            detectors.add(holder);
            if (defaultStr != null && key.equals(defaultStr)) {
                defaultEnum = holder;
            }
        }
//        if (defaultStr != null) {
//            detectors
//            defaultValue = list.indexOf(defaultStr);
//            if (defaultValue < 0)
//                System.out.println("[GetEnumDetect] Warning! Unknown default item: " + defaultStr);
//        }
    }

    public GetEnumDetect(JSONArray arr, String[] values) throws JSONException {
        List<String> list = Arrays.asList(values);
        for (int i = 0; i < arr.length(); i++) {
            JSONObject item = arr.getJSONObject(i);
            String key = JSON.getStringSafe(item, "name");// names.getString(i);
            int index = list.indexOf(key);
            if (index < 0) {
                System.out.println("[GetEnumDetect] Warning! Unknown enum item: " + key);
                continue;
            }
            Holder holder = new Holder(index, key, GetDetect.createDetect(item, "value"));
            if (item.has("default")) {
                defaultEnum = holder;//index;
//                continue;
            }
            detectors.add(holder);
        }
    }

    public GetEnumDetect(JSONObject obj) throws JSONException {
        this.getStart = GetDetect.createDetect(obj, "start");
        this.getEnd = GetDetect.createDetect(obj, "end");
        this.returnName = JSON.getBooleanSafe(obj, "return_name", false);
        String defaultName = JSON.getStringSafe(obj, "default");

        JSONObject enumObj = obj.getJSONObject("enum");
        JSONArray names = enumObj.names();
        for (int i = 0; i < names.length(); i++) {
            String name = names.getString(i);
            Holder holder = new Holder(i, name, GetDetect.createDetect(enumObj.get(name)));
            detectors.add(holder);
            if (defaultName != null && name.equals(defaultName)) {
                defaultEnum = holder;
            }
        }
    }

    public boolean isReturnName() {
        return returnName;
    }

    private Holder getHolder(StringBuilder str, int st) {
        int start = st >= 0 ? st : 0;
        int end = str.length();
        if (getStart != null) {
            start = getStart.getIndex(str, start);
            if (details)
                System.out.println("[add_object.reset][start]" + getStart.value + " " + start);
        }
        if (getEnd != null) {
            end = getEnd.getIndex(str, start + 1);
            if (details)
                System.out.println("[add_object.reset][end]" + getEnd.value + " " + end);
        }
        if (start >= 0 && end > start) {
            for (Holder holder : detectors) {
                GetDetect detector = holder.detector;
                if (detector.detect(str, start, end)) {
                    return holder;
                }
            }
        }
        if (defaultEnum != null)
            return defaultEnum;
        else
            return null;//def;
    }

    public int order(StringBuilder str) {
        Holder h = getHolder(str, 0);
        return (h != null) ? h.index : -1;
//        return get(str, -1);
    }

    public int order(StringBuilder str, int def, int st) {
        Holder h = getHolder(str, st);
        return (h != null) ? h.index : def;
//        for (Holder holder : detectors) {
//            GetDetect detector = holder.detector;
//            if (detector.detect(str)) {
//                return holder.index;// key;
//            }
//        }
//        if (defaultValue >= 0)
//            return defaultValue;
//        else
//            return def;
    }

    public Object get(StringBuilder str) {
        return get(str, -1);
    }

    public Object get(StringBuilder str, int i) {
        if (isReturnName())
            return name(str, i);
        else
            return order(str, -1, i);
    }

    public String name(StringBuilder str) {
        return name(str, 0);
    }

    public String name(StringBuilder str, int st) {
        Holder h = getHolder(str, st);
        return (h != null) ? h.name : null;
    }

    static class Holder {
        final int index;
        final String name;
        final GetDetect detector;

        Holder(int index, String name, GetDetect detector) {
            this.index = index;
            this.name = name;
            this.detector = detector;
        }
    }
}
