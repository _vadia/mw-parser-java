package org.vadel.libs.parse.engine;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by VBabin on 28.09.2017.
 */
@SuppressWarnings("unused")
public class AddObject {

    public enum FieldKind { _none, _bool, _string, _enum, _object, _array }

    public static AddObject createAddObject(JSONObject obj, String name) {
        if (obj == null)
            return null;
        try {
            JSONObject o = JSON.getObjectSafe(obj, name);
            if (o == null)
                return null;
            return new AddObject(o);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static AddObject createAddObject(JSONObject obj) {
        if (obj == null)
            return null;
        return new AddObject(obj);
    }

    private final List<Field> fields = new ArrayList<Field>();
    private int index, start, end;

    private GetDetect getStart, getNext, getEnd;
//    private Map<String, Object> fields = new HashMap<String, Object>();
    private boolean details;

    AddObject(JSONObject obj) {
        this.getStart = GetDetect.createDetect(obj, "start");
        this.getNext = GetDetect.createDetect(obj, "next");
        this.getEnd = GetDetect.createDetect(obj, "end");

        try {
            JSONObject fieldsObj = JSON.getObjectSafe(obj, "fields");

            JSONArray names = fieldsObj.names();
            for (int i = 0; i < names.length(); i++) {
                String name = names.getString(i);
                appendGetter(name, fieldsObj);
            }

            this.details = JSON.getBooleanSafe(obj, "details", details);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void appendGetter(String name, JSONObject fieldsObj) throws JSONException {
        Object o = fieldsObj.get(name);
        if (name.endsWith("[]")) {
            // array
            fields.add(new Field(FieldKind._array, name.substring(0, name.length() - 2),
                    AddObject.createAddObject(fieldsObj, name)));
        } else if (name.endsWith("{}")) {
            // object
            fields.add(new Field(FieldKind._object, name.substring(0, name.length() - 2),
                    AddObject.createAddObject(fieldsObj, name)));
        } else if (name.endsWith("()")) {
            // enum
            fields.add(new Field(FieldKind._enum, name.substring(0, name.length() - 2),
                    new GetEnumDetect(JSON.getObjectSafe(fieldsObj, name))));
        } else if (name.endsWith("?")) {
            // bool
            fields.add(new Field(FieldKind._bool, name.substring(0, name.length() - 1),
                    GetDetect.createDetect(fieldsObj, name)));
        } else {
            fields.add(new Field(FieldKind._string, name, GetString.createString(fieldsObj, name)));
        }
    }

    public void reset(StringBuilder str) {
        reset(str, 0);
    }

    public void reset(StringBuilder str, int position) {
        start = position;
        end = str.length();
        if (getStart != null) {
            start = getStart.getIndex(str, start);
            if (details)
                System.out.println("[add_object.reset][start]" + getStart.value + " " + start);
        }
        if (getEnd != null) {
            end = getEnd.getIndex(str, start + 1);
            if (details)
                System.out.println("[add_object.reset][end]" + getEnd.value + " " + end);
        }
        index = start;
    }

    public boolean next(StringBuilder str) {
        if (index < 0)
            return false;

        if (index > 0)
            index++;

        index = getNext.getIndex(str, index);

        if (details) {
            if (index >= 0) {
                String s;
                if (index + 50 < str.length())
                    s = str.substring(index, index + 100) + "...";
                else
                    s = str.substring(index);
                System.out.println("[add_object.next]" + index + " " + s);
            } else {
                System.out.println("[add_object.next][end]" + getNext.value);
            }
        }

        return index >= 0 && index <= end;
    }

    public JSONObject getObject(StringBuilder str) {
        try {
            AddObject addObject;
            JSONObject obj = new JSONObject();
            for (Field field : fields) {
                obj.put(field.name, field.get(str, index));
            }
            return obj;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    class Field {
        final FieldKind kind;
        final String name;
        final Object getter;

        public Field(FieldKind kind, String name, Object getter) {
            this.kind = getter == null ? FieldKind._none : kind;
            this.name = name;
            this.getter = getter;
        }

        GetString asGetString() {
            return (GetString) getter;
        }

        AddObject asAddString() {
            return (AddObject) getter;
        }

        GetEnumDetect asEnum() {
            return (GetEnumDetect) getter;
        }

        GetDetect asDetect() {
            return (GetDetect) getter;
        }

        public Object get(StringBuilder str, int index) {
            AddObject addObject;
            switch (kind) {
                case _object:
                    addObject = asAddString();
                    addObject.reset(str, index);
                    return addObject.getObject(str);

                case _array:
                    addObject = asAddString();
                    addObject.reset(str, index);
                    JSONArray array = new JSONArray();
                    while (addObject.next(str)) {
                        array.put(addObject.getObject(str));
                    }
//                        if (details) {
//                            System.out.println(array);
//                        }
                    return array;

                case _enum:
                    return asEnum().get(str, index);

                case _bool:
                    return asDetect().detect(str, index, end);

                case _string:
                    return asGetString().getValue(str, index);

                default:
                    if (details) {
                        System.out.println("Empty field: " + name);
                    }
                    return null;
            }

        }
    }
}
