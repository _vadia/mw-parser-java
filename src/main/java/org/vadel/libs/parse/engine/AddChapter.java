package org.vadel.libs.parse.engine;

import org.json.JSONException;
import org.json.JSONObject;

public class AddChapter {

    public static AddChapter createAddChapter(JSONObject obj, String name) {
        if (obj == null)
            return null;
        try {
            JSONObject o = JSON.getObjectSafe(obj, name);
            if (o == null)
                return null;
            return new AddChapter(o);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static AddChapter createAddChapter(JSONObject obj) {
        if (obj == null)
            return null;
        return new AddChapter(obj);
    }

    int index = 0, start = 0, end = 0;

    int getNextLen;
    GetDetect getStart, getNext, getEnd;
    GetString getLink, getTitle, getUniq;
    public boolean details;

    AddChapter(JSONObject obj) {
        this.getStart = GetDetect.createDetect(obj, "start");
        this.getNext = GetDetect.createDetect(obj, "next");
        this.getEnd = GetDetect.createDetect(obj, "end");
        this.getLink = GetString.createString(obj, "link");
        this.getTitle = GetString.createString(obj, "title");
        this.getUniq = GetString.createString(obj, "uniq");

        try {
            this.details = JSON.getBooleanSafe(obj, "details", details);
        } catch (JSONException e) {
            e.printStackTrace();
        }

//		this.getNextLen = getNext.value.length();
    }

    public void reset(StringBuilder str) {
        reset(str, 0);
    }

    public void reset(StringBuilder str, int position) {
        start = position;
        end = str.length();
        if (getStart != null) {
            start = getStart.getIndex(str, start);
            if (details)
                System.out.println("[add_chapter.reset][start]" + getStart.value + " " + start);
        }
        if (getEnd != null) {
            end = getEnd.getIndex(str, start + 1);
            if (details)
                System.out.println("[add_chapter.reset][end]" + getEnd.value + " " + end);
        }
        index = start;
    }

    public boolean next(StringBuilder str) {
        if (index < 0)
            return false;

        if (index > 0)
            index++;//= getNextLen;

        index = getNext.getIndex(str, index);

        if (details) {
            if (index >= 0) {
                String s;
                if (index + 50 < str.length())
                    s = str.substring(index, index + 100) + "...";
                else
                    s = str.substring(index);
                System.out.println("[add_chapter.next]" + index + " " + s);
            } else {
                System.out.println("[add_chapter.next][end]" + getNext.value);
            }
        }

        return index >= 0 && index <= end;
    }

    public String getLink(StringBuilder str) {
        String s = getLink.getValue(str, index);
        if (details) {
            System.out.println("[add_chapter.link]" + s);
        }
        return s;
    }

    public String getTitle(StringBuilder str) {
        String s = getTitle.getValue(str, index);
        if (details) {
            System.out.println("[add_chapter.title]" + s);
        }
        return s;
    }

    public String getUniq(StringBuilder str) {
        if (getUniq == null)
            return null;
        String s = getUniq.getValue(str, index);
        if (details) {
            System.out.println("[add_chapter.uniq]" + s);
        }
        return s;
    }
}