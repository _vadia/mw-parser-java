package org.vadel.libs.parse.engine;

/**
 * Created by VBabin on 27.06.2017.
 */
public class GetConstantString extends GetString {

    final String value;

    public GetConstantString(String value) {
        super();
        this.value = value;
    }

    @Override
    public boolean isValid() {
        return value != null;
    }

    @Override
    public String getValue(StringBuilder str, int i) {
        return value;
    }
/*
    @Override
    public List<String> getValues(StringBuilder str, int i) {
        List<String> result = new ArrayList<String>();
        if (value != null) {
            result.add(value);
            return result;
        }
        return result;
    }
*/
}
