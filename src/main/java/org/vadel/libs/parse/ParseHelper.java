package org.vadel.libs.parse;

import java.util.List;

public class ParseHelper {

    public static void getPagesList(final StringBuilder str, List<String> pages,
                                    String uniq, String token1, String token2, String tokenEnd, String beforeToken, String afterToken) {
        getPagesList(str, pages, uniq, token1, token2, tokenEnd, beforeToken, afterToken, 0);
    }

    public static void getPagesList(final StringBuilder str, List<String> pages,
                                    String uniq, String token1, String token2, String tokenEnd,
                                    String beforeToken, String afterToken, int i1) {
        i1 = str.indexOf(uniq, i1);
        if (i1 > 0) {
            int i2;
            int i3 = str.indexOf(tokenEnd, i1);
            i1 = str.indexOf(token1, i1);
            final int len1 = token1.length();
            while (i1 > 0 && i1 < i3) {
                i1 += len1;
                i2 = str.indexOf(token2, i1);
                if (i2 < 0)
                    break;
                pages.add(beforeToken + str.substring(i1, i2) + afterToken);
                i1 = str.indexOf(token1, i1);
            }
        }
    }

    public static String getLineValueAfterUniq(final String line, final String uniq, final String token1, final String token2) {
        String result = null;
        int i1, i2;
        i1 = line.indexOf(uniq);
        if (i1 >= 0) {
            i1 = line.indexOf(token1, i1 + uniq.length());
            i1 += token1.length();
            i2 = line.indexOf(token2, i1);
            if (i2 > i1)
                result = line.substring(i1, i2).trim();
        }
        return result;
    }

    public static String getLineValueSB(StringBuilder str, String uniq, String token1, String token2, int i) {
        int i1, i2;
        i1 = str.indexOf(uniq, i);
        if (i1 > 0) {
            i1 = str.indexOf(token1, i1);
            i1 += token1.length();
            i2 = str.indexOf(token2, i1);
            if (i1 >= 0 && i2 >= 0)
                return str.substring(i1, i2);
        }
        return null;
    }
}
