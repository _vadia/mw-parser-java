package org.vadel.libs.parse.engine;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import java.io.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by VBabin on 28.09.2017.
 */
public class AddObjectTest {

    StringBuilder getStringFromTestFile(String name) throws UnsupportedEncodingException {
//        System.out.println(getClass().getClassLoader().getResource(name).getFile());
//        return null;
        InputStream in = getClass().getClassLoader().getResourceAsStream(name);

        Reader fr = new InputStreamReader(in, "utf-8");
        try {
            StringBuilder str = new StringBuilder();
            char[] buff = new char[512];
            int n;
            while ((n = fr.read(buff)) > 0) {
                str.append(buff, 0, n);
            }
            return str;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    // @Test
    public void testObjects() throws JSONException, UnsupportedEncodingException {
        StringBuilder o = getStringFromTestFile("test1.json");
        assertNotNull(o);
        AddObject addObject = AddObject.createAddObject(new JSONObject(o.toString()), "items");
        assertNotNull(addObject);
        StringBuilder str = getStringFromTestFile("test1.html");
        assertNotNull(str);
        int n = 0;
        addObject.reset(str);
        while (addObject.next(str)) {
            JSONObject obj = addObject.getObject(str);
            assertTrue(obj.has("array"));
            assertEquals(5, obj.getJSONArray("array").length());
            System.out.println(obj.toString());
            assertNotNull(obj);
            n++;
        }
        assertEquals(2, n);
    }
}
