package org.vadel.libs.parse.engine;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by VBabin on 13.10.2015.
 */
public class GetDetectTest {

    String TEST_STRING = "<ul>\n"+
            "<li><a href=\"/wiki/Vladimir_Nabokov\" title=\"Vladimir Nabokov\">Vladimir Nabokov</a> (1899–1977)</li>\n"+
            "<li>adasd asdasd<a href=\"/wiki/Linda_Nagata\" title=\"Linda Nagata\">Linda Nagata</a> (born 1960)</li>";

    // @Test
    public void testValue() throws JSONException {
        GetDetect get = new GetDetect(new JSONObject("{ 'value': '<a href=\"' }"));
        assertTrue( get.getIndex(new StringBuilder(TEST_STRING)) > 0);
    }

    // @Test
    public void testPattern() throws JSONException {
        GetDetect get = new GetDetect(new JSONObject("{ 'pattern': '<li>.*<a href=\"' }"));
        int i1 = get.getIndex(new StringBuilder(TEST_STRING));
        assertTrue(i1 > 0);
        int i2 = get.getIndex(new StringBuilder(TEST_STRING), i1 + 1);
        assertTrue(i2 > 0);
        System.out.println(String.format("detect: %d -> %d", i1, i2));
    }
}
